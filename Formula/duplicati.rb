class Duplicati < Formula
  desc "Free backup software to store encrypted backups online"
  homepage "https://www.duplicati.com"
  version "2.0.4.5_beta_2018-11-28"
  url "https://github.com/duplicati/duplicati/releases/download/v2.0.4.5-2.0.4.5_beta_2018-11-28/duplicati-2.0.4.5_beta_2018-11-28.zip"
  sha256 "1351b135cb366823a24c4fda256843ea8fe6d0138ff75dcd05622e9e0a84ea2b"

  depends_on "robywan/cloud/mono"

  def install
    dest = lib/"duplicati"

    dest.mkpath
    dest.install Dir["*"]

    (libexec/"duplicati-cli").write cli_wrapper
    chmod 0755, (libexec/"duplicati-cli")
    bin.install_symlink (libexec/"duplicati-cli")

    (libexec/"duplicati-server").write server_wrapper
    chmod 0755, (libexec/"duplicati-server")
    bin.install_symlink (libexec/"duplicati-server")
  end

  def post_install
    datadir.mkpath
  end

  test do
    system "duplicati-cli", "system-info"
  end

  def datadir
    var/"duplicati"
  end

  def cli_wrapper
    <<~EOS
      #!/usr/bin/env bash
      INSTALLDIR=#{lib}/duplicati
      export LD_LIBRARY_PATH="${INSTALLDIR}${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
      export MONO_PATH=$MONO_PATH:${INSTALLDIR}

      EXE_FILE=${INSTALLDIR}/Duplicati.CommandLine.exe
      APP_NAME=Duplicati.CommandLine

      exec -a "$APP_NAME" #{Formula["mono"].opt_prefix}/bin/mono "$EXE_FILE" "$@"
    EOS
  end

  def server_wrapper
    <<~EOS
      #!/usr/bin/env bash
      INSTALLDIR=#{lib}/duplicati
      export LD_LIBRARY_PATH="${INSTALLDIR}${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
      export MONO_PATH=$MONO_PATH:${INSTALLDIR}

      EXE_FILE=${INSTALLDIR}/Duplicati.Server.exe
      APP_NAME=DuplicatiServer

      exec -a "$APP_NAME" #{Formula["mono"].opt_prefix}/bin/mono "$EXE_FILE" "$@"
    EOS
  end
end
