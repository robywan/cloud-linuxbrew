require 'formula'

class Composer < Formula
  desc "Dependency Manager for PHP"
  homepage "https://getcomposer.org"
  url "https://getcomposer.org/download/1.6.3/composer.phar"
  sha256 "52cb7bbbaee720471e3b34c8ae6db53a38f0b759c06078a80080db739e4dcab6"
  head "https://getcomposer.org/composer.phar"

  bottle do
    cellar :any_skip_relocation
    rebuild 1
    sha256 "cd7d7688f5c54609012418e9f3f6e8e19d66428dbb3ef959f52b0257770571a3" => :high_sierra
    sha256 "cd7d7688f5c54609012418e9f3f6e8e19d66428dbb3ef959f52b0257770571a3" => :sierra
    sha256 "cd7d7688f5c54609012418e9f3f6e8e19d66428dbb3ef959f52b0257770571a3" => :el_capitan
  end

  #depends_on PharRequirement

  def install
    if phar_file == phar_bin
      @real_phar_file = phar_file + ".phar"
      File.rename(phar_file, @real_phar_file)
    else
      @real_phar_file = phar_file
    end

    libexec.install @real_phar_file
    (libexec/phar_bin).write phar_wrapper
    chmod 0755, (libexec/phar_bin)
    bin.install_symlink (libexec/phar_bin)
  end

  test do
    system "#{bin}/composer", "--version"
  end

  def phar_file
    class_name = self.class.name.split("::").last
    class_name.downcase + ".phar"
  end

  def phar_bin
    class_name = self.class.name.split("::").last
    class_name.downcase
  end

  def phar_wrapper
    <<~EOS
      #!/usr/bin/env php
      <?php
      array_shift($argv);
      $arg_string = implode(' ', array_map('escapeshellarg', $argv));
      $arg_prefix = preg_match('/--(no-)?ansi/', $arg_string) ? '' : '--ansi ';
      $arg_string = $arg_prefix . $arg_string;
      passthru("/usr/bin/env php -d allow_url_fopen=On -d detect_unicode=Off #{libexec}/#{@real_phar_file} $arg_string", $return_var);
      exit($return_var);
    EOS
  end
end
