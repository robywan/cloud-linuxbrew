class Emailrelay < Formula
  desc "E-MailRelay is a simple SMTP proxy and store-and-forward message transfer agent (MTA)"
  homepage "http://emailrelay.sourceforge.net/"
  url "https://netix.dl.sourceforge.net/project/emailrelay/emailrelay/2.0/emailrelay-2.0-src.tar.gz"
  #mirror "https://fossies.org/linux/privat/monit-5.23.0.tar.gz"
  sha256 "0b3a98edfdc8deaf2b3d14e2d2a7fa8402f6703d83ac7458653994124de6a587"

  #option "without-pam", "Disable PAM support"

  depends_on "openssl"

  def install
    args = %W[
      --prefix=#{prefix}
      --exec-prefix=#{prefix}
      --datadir=#{share}
      --localstatedir=#{var}
      --libexecdir=#{lib}
      --sysconfdir=#{etc}
      --disable-gui
      --with-openssl=#{Formula["openssl"].opt_prefix}
    ]

    system "./configure", *args
    system "make", "install"
  end

  #test do
  #  system bin/"monit", "-c", pkgshare/"monitrc", "-t"
  #end
end