class Monit < Formula
  desc "Manage and monitor processes, files, directories, and devices"
  homepage "https://mmonit.com/monit/"
  url "https://mmonit.com/monit/dist/monit-5.25.1.tar.gz"
  sha256 "4b5c25ceb10825f1e5404f1d8a7b21507716b82bc20c3586f86603691c3b81bc"

  bottle do
    root_url "http://brew.dev.ricci-consulting.com"
    sha256 "e7b89aea7c2cf3c7d08755919eb940f0cbe5ede1efc5b8f0966aa2177fb12a64" => :x86_64_linux
  end

  option "with-pam", "Enable PAM support"

  depends_on "openssl"

  def install
    args = %W[
      --prefix=#{prefix}
      --localstatedir=#{var}/monit
      --sysconfdir=#{etc}/monit
      --with-ssl-dir=#{Formula["openssl"].opt_prefix}
    ]

    args << "--without-pam" unless build.with? "pam"

    system "./configure", *args
    system "make", "install"
    pkgshare.install "monitrc"
  end

  test do
    system bin/"monit", "-c", pkgshare/"monitrc", "-t"
  end
end